package tp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activity;

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public MonitoredData() {
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    private final String fileName = "D:\\An_3\\Semestrul_2\\TP_Restanta\\Tema5\\Activities.txt";

    public List<MonitoredData> readData(){
        List<MonitoredData> dataRead = new ArrayList<>();

        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));

            dataRead = stream.map(line -> line.split("\t"))
                         .map(a -> new MonitoredData(a[0],a[2],a[4]))
                         .collect(Collectors.toList());


        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataRead;
    }

    public Long countDays(List<MonitoredData> activities){
        Long noOfDays;

        String startDate;
        String endDate;

        String[] output1;
        String[] output2;

        output1 = activities.get(0).getStartTime().split(" ");
        startDate = output1[0];

        output2 = activities.get(activities.size()-1).getEndTime().split(" ");
        endDate = output2[0];

        LocalDate dateStart = LocalDate.parse(startDate);
        LocalDate dateEnd = LocalDate.parse(endDate);

        noOfDays = ChronoUnit.DAYS.between(dateStart,dateEnd);

        return noOfDays;
    }

    public HashMap<String,Integer> getActivities(List<MonitoredData> activities){
        HashMap<String,Integer> distinctActivities = new HashMap<>();


        for (MonitoredData activity : activities){
            distinctActivities.put(activity.getActivity(),0);
        }

        activities.stream()
                .map(f -> {
                    Integer count = distinctActivities.get(f.getActivity());
                    distinctActivities.replace(f.getActivity(),count + 1);

                    return null;
                })
                .collect(Collectors.toList());

        return distinctActivities;
    }

    public static void main(String[] args) {
        MonitoredData data = new MonitoredData();

        List<MonitoredData> activities;
        HashMap<String,Integer> distinctActivities;

        Long days;

        activities = data.readData(); //2 points

        days = data.countDays(activities); //1 point

        distinctActivities = data.getActivities(activities); // 2 points

        System.out.println("1) All activities \n");
        for ( MonitoredData activity : activities){
            System.out.println(activity.toString());
        }
        System.out.println("\n");

        System.out.println("2) Number of days \n");
        System.out.println(days+1);
        System.out.println("\n");

        System.out.println("3) Different activities \n");
        for (Map.Entry<String, Integer> entry : distinctActivities.entrySet())
            System.out.println("	" + entry.getKey() + ": " + entry.getValue().toString());

        System.out.println("\n");

    }
}
